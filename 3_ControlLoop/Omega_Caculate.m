function [Omega] = Omega_Caculate(Step,ts,Omega_In,Run_Flag)
%OMEGA_CACULATE Summary of this function goes here
%   Detailed explanation goes here
% Starta_Time  开始时间
% Stop_Time    停止时间
% ts           采样时间
% Omega_In(Row,Line)     期望输入角速率
% Line1 动作起始时间
% Line2 动作终止时间
% Line3 动作状态  0为匀速 1为加速
% Line4 动作运行值
% Run_Flag 运行标志 1为静止 2为输入Omega_In值，3为输出正弦值
if Run_Flag==1
    for k=1:1:Step
       time(k)=k*ts;
       Omega(k,1)=time(k);
       Omega(k,2)=0;
    end
elseif Run_Flag==2
    [Row,Line]=size(Omega_In);
    Row_Count=1;
    Omega=zeros(Step,2);
    for k=1:1:Step
        time(k)=k*ts;
        Omega(k,1)=time(k);
        if ((time(k)>=Omega_In(Row_Count,1))&&((time(k)<Omega_In(Row_Count,2))))
            switch Omega_In(Row_Count,3)
                case 0
                    Omega(k,2)=Omega_In(Row_Count,4);
                case 1
                    Omega(k,2)=((Omega_In(Row_Count,4)-Omega_In(Row_Count-1,4))/((Omega_In(Row_Count,2)-Omega_In(Row_Count,1))))*(time(k)-Omega_In(Row_Count,1))+Omega_In(Row_Count-1,4);
            end
        elseif time(k)==Omega_In(Row_Count,2)
            Omega(k,2)=Omega_In(Row_Count,4);
            Row_Count=Row_Count+1;
        end
    end
elseif Run_Flag==3
     for k=1:1:Step
       time(k)=k*ts;
       Omega(k,1)=time(k);
       Omega(k,2)=360*pi/180*sin(2*pi*1*time(k));
    end
end
end

