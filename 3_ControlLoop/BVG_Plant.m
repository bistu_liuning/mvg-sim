function [A1,B1,C1,D1,R_Omega] = BVG_Plant(Modal_Flag,Fre_X,Fre_Y,ts,time,Omega_In)
%BVG_PLANT Summary of this function goes here
% Modal_Flag 模型选择标识
% 振动陀螺被控对象仿真模型
%1 定义状态控制矩阵形式摘自文献：Development of LabVIEW Programs for Simulating Resonator
%Gyros
%2 定义状态控制矩阵形式摘自文献：A Novel Adaptive Sliding Mode Controller for MEMS
%Gyroscope
% Fre_X x轴振动频率
% Fre_Y y轴振动频率
% ts 离散化时间，即系统采样时间
% time 系统运行时间
%   Detailed explanation goes here
%% 根据时间改变输入角速率
R_Omega=Omega_In(fix(time/ts),2);
%% 计算状态空间模型
if Modal_Flag==1
    k=0.6;      %进动因子
    omega1=Fre_X*2*pi;  %x轴振动频率
    omega2=Fre_Y*2*pi;  %y轴振动频率
    tau1=30;
    tau2=18;
    theta_omega=2/57.3;%degree_to_rad
    theta_tau=3/57.3;
    omega_2=(omega1^2+omega2^2)/2;
    omega_delta_omega=(omega1^2-omega2^2)/2;
    t1_tau=(1/tau1+1/tau2)/2;
    delta_tau=(1/tau1-1/tau2)/2;
    a21=-omega_2+omega_delta_omega*cos(2*theta_omega);
    a22=-2*t1_tau-delta_tau*cos(2*theta_tau);
    a23=omega_delta_omega*sin(2*theta_omega);
    a24=2*k*R_Omega-delta_tau*sin(2*theta_tau);
    a41=omega_delta_omega*sin(2*theta_omega);
    a42=-2*k*R_Omega-delta_tau*sin(2*theta_tau);
    a43=-omega_2-omega_delta_omega*cos(2*theta_omega);
    a44=-2*t1_tau+delta_tau*cos(2*theta_tau);
    %生成状态空间变量
    A=[  0   1   0   0;
       a21 a22 a23 a24;
         0   0   0   1;
       a41 a42 a43 a44];
    B=[0 0;
      1 0;
      0 0;
      0 1];
    C=[1 0 0 0;
       0 0 1 0;];
    D=[0 0;0 0];
elseif Modal_Flag==2
   omegaz=R_Omega;
   m=0.57e-8;
   dxx=0.429e-6;   %阻尼系数
   dxy=0.0429e-6;
   dyy=0.687e-6;
   kxx=80.98;      %弹簧系数
   kxy=5;
   kyy=71.62;
   w0=6635*2*pi;
   q0=1e-6;
   wx=(kxx/(m*w0*w0))^0.5; %x轴振动频率
   wy=(kyy/(m*w0*w0))^0.5; %y轴振动频率
   wxy=kxy/(m*w0*w0);
   A=[0  1  0  0;
      -wx*wx  -dxx  -wxy  -(dxy-2*omegaz);
      0  0  0  1;
      -wxy    -(dxy+2*omegaz)  -wy*wy  -dyy;
      ];
   B=[0 0;
      1 0;
      0 0;
      0 1];
   C=[1 0 0 0;
       0 0 1 0;];
   D=[0 0;0 0];
elseif Modal_Flag==3
   
   k=0.6;
   omega=R_Omega;
   omega1=Fre_X*2*pi;  %x轴振动频率
   omega2=Fre_Y*2*pi;  %y轴振动频率
   kp=omega1^2;
   kq=omega2^2;
   dp=4;
   dq=3;
   kpq=507580;
   kqp=507580;
   A=[0    1     0           0;
      -kp  -dp  -kpq  -4*k*omega;
      0      0     0          1;
      -kqp  4*k*omega  -kq  -dq;
      ];
   B=[0 0;
      1 0;
      0 0;
      0 1];
   C=[1 0 0 0;
       0 0 1 0;];
   D=[0 0;0 0];     
end
[A1,B1,C1,D1]=c2dm(A,B,C,D,ts,'z'); %离散化被控对象
end

