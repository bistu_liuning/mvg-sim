%% 振动陀螺回路仿真分析
%  功能描述：重点仿真 振动陀螺振动回路的操作时序，控制算法
%  创建日期：20121120
%  创建人：刘宁
%  版权：北京信息科技大学智能控制研究所；北京理工大学
%  备注：1.采用离散仿真方法，避免采用s函数方法造成的仿真时间过长问题
%  修改记录：20121128 进行修改，加入控制回路
%           20121213 修改控制回路
%  版本：1.2
%% 系统初始化
clear all;
clc;
close all;
%% 定义振动陀螺仿真模型
Modal_Flag=1;
% 振动陀螺被控对象仿真模型
%1 定义状态控制矩阵形式摘自文献：Development of LabVIEW Programs for Simulating Resonator
%Gyros
%2 定义状态控制矩阵形式摘自文献：A Novel Adaptive Sliding Mode Controller for MEMS
%Gyroscope
Control_Flag=3;
%1 无控
Fre_X=6635.6;
Fre_Y=6635.1;
ts=0.005;                          %系统采样时间
Start_Time=0;
Stop_Time=66;
Step=(Stop_Time-Start_Time)/ts;  %仿真步数
% Omega_In(Row,Line)     期望输入角速率
% Line1 动作起始时间
% Line2 动作终止时间
% Line3 动作状态  0为匀速 1为加速
% Line4 动作运行值,单位为弧度
Omega_In=[0 2 0 0;
          2 3 1 300/(180/pi);
          3 6 0 300/(180/pi);
          6 7 1 0;
          7 10 0 0;
          10 11 1 -300/(180/pi);
          11 14 0 -300/(180/pi);
          14 15 1 0;
          15 18 0 0;
          18 22 0 0;
          ];
% Omega_In=[0 2 0 0;
%           2 3 1 300/(180/pi);
%           3 14 0 300/(180/pi);
%           14 15 1 0;
%           15 18 0 0;
%           ];

Run_Flag=1;   %选择输入角速率生成格式
[Omega]=Omega_Caculate(Step,ts,Omega_In,Run_Flag);  %获取角速率输入曲线
%% 参数初始化
u=zeros(2,1);
x=[0;0;0;0]; %定义初始状态变量
Fx_In=zeros(fix(Step),1);
Fy_In=zeros(fix(Step),1);
X_out=zeros(fix(Step),1);
Y_out=zeros(fix(Step),1);
XK=zeros(fix(Step),4);
Module=zeros(fix(Step),1);
Demodule=zeros(fix(Step),1);
Mag_X=zeros(fix(Step),1);  %x轴输出幅值
Phi_X=zeros(fix(Step),1);  %x轴相位
Mag_Y=zeros(fix(Step),1);  %y轴幅值
Phi_Y=zeros(fix(Step),1);  %y轴相位
%调制解调低通滤波器
%滤波器参数
s1=0.020083365564211236;
a21=-1.5610180758007182;
a31=0.64135153805756318;
b21=2;
State1_X=zeros(2,1);
State2_X=zeros(2,1);
State1_Y=zeros(2,1);
State2_Y=zeros(2,1);
Order=51;            %FIR阶数
State_1=zeros(Order,1); %生成状态矩阵
B_k_1=zeros(Order,1);
Temp_Fir_1=zeros(Order,1);

State_2=zeros(Order,1); %生成状态矩阵
B_k_2=zeros(Order,1);
Temp_Fir_2=zeros(Order,1);

State_3=zeros(Order,1); %生成状态矩阵
B_k_3=zeros(Order,1);
Temp_Fir_3=zeros(Order,1);

State_4=zeros(Order,1); %生成状态矩阵
B_k_4=zeros(Order,1);
Temp_Fir_4=zeros(Order,1);

B_k_1=[-0.00107673146381826,0.000634831821018186,0.00184100911914794,0.00132235040818464,-0.00102132993220374,-0.00350820806156636,-0.00359981732580948,-8.17789201313502e-05,0.00520505313387509,0.00793468010878225,0.00444058607783886,-0.00461387517672435,-0.0132277670803601,-0.0135062932122784,-0.00196068220854419,0.0158792885248372,0.0269780708359160,0.0190624769468328,-0.00898065062864987,-0.0420331787601041,-0.0541738597597105,-0.0231814118811653,0.0540355582727353,0.155212966490062,0.241074953062299,0.274703105197490,0.241074953062299,0.155212966490062,0.0540355582727353,-0.0231814118811653,-0.0541738597597105,-0.0420331787601041,-0.00898065062864987,0.0190624769468328,0.0269780708359160,0.0158792885248372,-0.00196068220854419,-0.0135062932122784,-0.0132277670803601,-0.00461387517672435,0.00444058607783886,0.00793468010878225,0.00520505313387509,-8.17789201313502e-05,-0.00359981732580948,-0.00350820806156636,-0.00102132993220374,0.00132235040818464,0.00184100911914794,0.000634831821018186,-0.00107673146381826;];
B_k_2=B_k_1;
B_k_3=B_k_2;
B_k_4=B_k_3;

diff_X=zeros(fix(Step),1);
diff_Y=zeros(fix(Step),1);
Omega_Out=zeros(fix(Step),1);   %获取角速率信息

fx=0;
fy=0;

%% 控制器设置
Con_Amp_x=zeros(Step,1);
Con_Amp_y=zeros(Step,1);
Con_Pha_x=zeros(Step,1);
Con_Fre=zeros(Step,1);
Con_Rate=zeros(Step,1);
Con_Qua=zeros(Step,1);
Omega_Test=zeros(Step,1);
P_Amp_x=0.9;
I_Amp_x=0.02;
P_Amp_y=1.6;
I_Amp_y=0.8;
P_Fre=0.02;
I_Fre=0.0003;
P_Rate=0.003;
I_Rate=0.008;
P_Qua=0.03;
I_Qua=0.08;
%% 算法仿真
for k=1:1:Step
    time(k)=k*ts;      %系统时间
    [A1,B1,C1,D1,Omega_Out(k)] = BVG_Plant(Modal_Flag,Fre_X,Fre_Y,ts,time(k),Omega);
    %生成调制解调器
    Module=sin(2*pi*Fre_X*time(k));
    Demodule=cos(2*pi*Fre_X*time(k));
    u(1)=fx;
    u(2)=fy;
    w=0;
    %进入状态方程迭代
    x=A1*x+B1*u;
    z=C1*x+w;
    
    %记录输入输出数据
    Fx_In(k)=fx;
    Fy_In(k)=fy;
    X_out(k)=z(1);
    Y_out(k)=z(2);
    
    %计算X输入输出相位幅值
    AX=X_out(k)*Module;
    AY=X_out(k)*Demodule;
    State_1(1)=AX;
    output_x=0;
    for i=1:1:Order
        Temp_Fir_1(i)=State_1(i)*B_k_1(i);
        output_x=output_x+Temp_Fir_1(i);
    end
    for i=Order:-1:2
        State_1(i)=State_1(i-1);
    end
    AX=output_x;
    State_2(1)=AY;
    output_y=0;
    for i=1:1:Order
        Temp_Fir_2(i)=State_2(i)*B_k_2(i);
        output_y=output_y+Temp_Fir_2(i);
    end
    for i=Order:-1:2
        State_2(i)=State_2(i-1);
    end
    AY=output_y;
    %计算X轴幅值相位
    Mag_X(k)=2*(AX^2+AY^2)^0.5;
    if AX~=0
       Phi_X(k)=atan(-AY/AX);
    elseif(AX==0)
        if(AY>0)
            Phi_X(k)= pi/2;
        elseif(AY<0)
            Phi_X(k)= -pi/2;
        end
    end
    %选取主值区间
    if (AX<0)
        if(AY>0)
            Phi_X(k)= Phi_X(k)+pi;
        elseif(AY<0)
            Phi_X(k)= Phi_X(k)-pi;
        end
    end
    if (AY==0)
         if(AX>0)
            Phi_X(k)= 0;
        elseif(AX<0)
            Phi_X(k)= -pi;
        end       
    end
    
    %计算Y输入输出相位幅值
    AX=Y_out(k)*Module;
    AY=Y_out(k)*Demodule;
    
    State_3(1)=AX;
    output_x=0;
    for i=1:1:Order
        Temp_Fir_3(i)=State_3(i)*B_k_3(i);
        output_x=output_x+Temp_Fir_3(i);
    end
    for i=Order:-1:2
        State_3(i)=State_3(i-1);
    end
    AX=output_x;
    State_4(1)=AY;
    output_y=0;
    for i=1:1:Order
        Temp_Fir_4(i)=State_4(i)*B_k_4(i);
        output_y=output_y+Temp_Fir_4(i);
    end
    for i=Order:-1:2
        State_4(i)=State_4(i-1);
    end
    AY=output_y;
    %计算Y轴幅值相位
    Mag_Y(k)=2*(AX^2+AY^2)^0.5;
    if AX~=0
       Phi_Y(k)=atan(-AY/AX);
    elseif(AX==0)
        if(AY>0)
            Phi_Y(k)= pi/2;
        elseif(AY<0)
            Phi_Y(k)= -pi/2;
        end
    end
    %选取主值区间
    if (AX<0)
        if(AY>0)
            Phi_Y(k)= Phi_Y(k)+pi;
        elseif(AY<0)
            Phi_Y(k)= Phi_Y(k)-pi;
        end
    end
    if (AY==0)
         if(AX>0)
            Phi_Y(k)= 0;
        elseif(AX<0)
            Phi_Y(k)= pi;
        end       
    end
    
%% 控制回路设计
if Control_Flag==1
    fx=(1000000)*sin(2*pi*(Fre_X)*time(k));
    fy=0;%1000000*sin(2*pi*(Fre_X)*time(k)+pi/2);
elseif Control_Flag==2   %选取控制回路运行模式
    % 振幅控制
    Err_Amp_x(k)=(fx-x(1)*1e9);  %计算振幅控制误差      
    Err_Amp_y(k)=0-x(3)*1e9;  %y轴振幅控制
    Err_Rate_y(k)=Mag_Y(k)*sin(Phi_X(k)-Phi_Y(k))*1e9;  %计算振幅误差
    Err_Qua_y(k)=Mag_Y(k)*cos(Phi_X(k)-Phi_Y(k))*1e9;  %计算振幅误差
    if k>1
       Con_Amp_x(k)=P_Amp_x*Err_Amp_x(k)+I_Amp_x*(Err_Amp_x(k)+Err_Amp_x(k-1));
       Con_Amp_y(k)=P_Amp_y*Err_Amp_y(k)+I_Amp_x*(Err_Amp_y(k)+Err_Amp_y(k-1));
       Con_Rate(k)=(Err_Rate_y(k)*P_Rate+I_Rate*(Err_Rate_y(k)+Err_Rate_y(k-1)));
       Con_Qua(k)=(Err_Qua_y(k)*P_Qua+I_Qua*(Err_Qua_y(k)+Err_Qua_y(k-1)));
    end
    fx=(1000000)*sin(2*pi*(Fre_X)*time(k))-Con_Amp_x(k);
    fy=(0.1)*(0-Con_Amp_y(k)-(Con_Rate(k)+ Con_Qua(k))*sin(2*pi*(Fre_X)*time(k)));    
elseif Control_Flag==3  %幅值频率控制
    P_Amp_x=32;
    I_Amp_x=0.01;
    P_Amp_y=1.6;
    I_Amp_y=0.8;
    P_Fre=0.001;
    I_Fre=0.0003;
    P_Rate=0.003;
    I_Rate=0.008;
    P_Qua=0.03;
    I_Qua=0.08;
    Err_Amp_x(k)=0.02-Mag_X(k);  %计算振幅控制误差 
    Err_Pha_x(k)=pi/2-Phi_X(k);
    Err_Amp_y(k)=0-x(3);  %y轴振幅控制
    Err_Rate_y(k)=Mag_Y(k)*cos(Phi_X(k)-Phi_Y(k));  %计算振幅误差
    Err_Qua_y(k)=Mag_Y(k)*sin(Phi_X(k)-Phi_Y(k));  %计算振幅误差
    if k>1
       Con_Amp_x(k)=P_Amp_x*Err_Amp_x(k)+I_Amp_x*(Err_Amp_x(k)+Err_Amp_x(k-1));
       Con_Pha_x(k)=P_Fre*Err_Pha_x(k)+I_Fre*(Err_Pha_x(k)+Err_Pha_x(k-1));
       Con_Rate(k)=(Err_Rate_y(k)*P_Rate+I_Rate*(Err_Rate_y(k)+Err_Rate_y(k-1)));
       Con_Qua(k)=(Err_Qua_y(k)*P_Qua+I_Qua*(Err_Qua_y(k)+Err_Qua_y(k-1)));
       Con_Amp_y(k)=P_Amp_y*Err_Amp_y(k)+I_Amp_x*(Err_Amp_y(k)+Err_Amp_y(k-1));
    end
    fx=(1e6)*(1-Con_Amp_x(k))*sin(2*pi*(Fre_X+Con_Pha_x(k))*time(k));
    fy=(1e9)*(0-Con_Amp_y(k)-(Con_Qua(k)+Con_Rate(k))*sin(2*pi*(Fre_X)*time(k)));
    Omega_Test(k)=(Con_Rate(k)*1e9+181.7)/21.56;
end
%% 控制回路输出
    %存储状态信息
     XK(k,1)=x(1);
     XK(k,2)=x(2);
     XK(k,3)=x(3);
     XK(k,4)=x(4);    
end
%% 显示输出
% figure(1);
% subplot(2,1,1);
% plotyy(time,Fx_In,time,X_out);
% legend('Fx','X output');
% ylabel('X output');
% title('Sin Response Curve');
% subplot(2,1,2);
% plotyy(time,Fy_In,time,Y_out);
% legend('Fx','Y output');
% ylabel('Y output');
% xlabel('Time(Second)')

figure(2)
subplot(2,1,1);
plot(time,X_out,'r',time,Mag_X,'b');
legend('X轴信号输出','X轴输出幅值');
ylabel('X output');
title('Sin Response Curve');
subplot(2,1,2);
plot(time,Y_out,'r',time,Mag_Y,'b');
legend('Y轴信号输出','Y轴输出幅值');
ylabel('Y output');
xlabel('Time(Second)');

figure(3)
plot(time,Phi_X*180/pi,'r',time,Phi_Y*180/pi,'b');
legend('x轴相位','y轴相位');
xlabel('Time(Second)');
title('相位输出示意图');
% 
% figure(4)
% plot(X_out(end-100:end),Y_out(end-100:end),'*');
% 
Point=150;
figure(5)
plot(time(1:end-Point),Omega_Out(1:end-Point)*180/pi,time(Point+1:end)-time(Point+1),Omega_Test(Point+1:end));
legend('输入角速度','解算角速度');
xlabel('Time(Second)');
title('解算结果');
a=Omega_Out(1:end-Point)*180/pi;
b=(Omega_Test(Point+1:end));
tt=1:1:length(a);
figure(6)

plot(tt,a,'b');
hold on
plot(tt,b,'r');

fid=fopen('x_y_out.dat','wt');
for i=1:1:Step
    fprintf(fid,'%f,%f,%f,%f,%f,%f,%f\n',time(i),X_out(i),Mag_X(i),Y_out(i),Mag_Y(i),Omega_Out(i)*180/pi,Omega_Test(i));
end
fclose(fid);

