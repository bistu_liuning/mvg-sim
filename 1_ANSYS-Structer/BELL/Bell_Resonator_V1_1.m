%% 钟形振子的结构设计与仿真
% 创建日期：20130321
% 创建人：刘宁
% 备注：

%% 系统初始化
clear all;
clc;
close all;
%% 定义结构参数
L1=0; %待定
L2=22E-3;  %振子整体高度
L3=15.8E-3;  %隔离孔距底部边缘高度
L4=3E-3;
L5=20E-3;
R1=10E-3;
R2=2E-3;
R3=1.5E-3;
R4=1.5E-3;
R5=4E-3;
R6=9E-3;
R7=2.5E-3;
R8=2.2E-3;
H1=0.5E-3;

GLUE_P=L2-L3; %粘胶位置，距底部高度
PIE_P=GLUE_P; %压电片位置，距底部高度
PIE_L=8e-3;   %压电片长
PIE_W=2E-3;   %压电片宽
PIE_H=0.2E-3; %压电片厚度
GLUE_L=PIE_L; %胶层长
GLUE_W=PIE_W; %胶层宽
GLUE_H=0.2E-3;%胶层厚度

fidin=fopen('Bell_APDL.txt','wt');
fprintf(fidin,'FINISH\n');
fprintf(fidin,'/CLEAR\n');
%打开ANSYS建模模块
fprintf(fidin,'/PREP7\n');
%% 绘制关键点
K(1,1)=0;
K(1,2)=0;
K(2,1)=0;
K(2,2)=L2-L4-sqrt(R6^2-R7^2);
K(3,1)=R2;
K(3,2)=K(2,2)+sqrt((R6-H1)^2-R2^2);
K(4,1)=R3;
K(4,2)=L2;
K(5,1)=R8;
K(5,2)=L2;
K(6,1)=R7;
K(6,2)=L2-L4;
K(7,1)=R6;
K(7,2)=K(2,2);
K(8,1)=R6;
K(8,2)=L2-L5;
K(9,1)=R6-H1;
K(9,2)=L2-L5;
K(10,1)=R6-H1;
K(10,2)=K(2,2);
Point_Num=10;
%绘制外圆
Circle_Num=6;
Step=(K(6,2)-K(7,2))/Circle_Num;
for i=1:1:Circle_Num-1
    Point_Num=Point_Num+1;
    K(Point_Num,2)=K(6,2)-Step*i;
    K(Point_Num,1)=sqrt(R6^2-(K(Point_Num,2)-K(2,2))^2);
end
%绘制内圆
Step=(K(3,2)-K(10,2))/Circle_Num;
for i=1:1:Circle_Num-1
    Point_Num=Point_Num+1;
    K(Point_Num,2)=K(3,2)-Step*i;
    K(Point_Num,1)=sqrt((R6-H1)^2-(K(Point_Num,2)-K(2,2))^2);
end
%绘制双曲线
a=R6;  %外侧
b2=((H1-L2+L5)^2)/((R1^2/R6^2)-1);
Hyper_Num=6;
Step=(K(8,2))/Hyper_Num;
for i=1:1:Hyper_Num
   Point_Num=Point_Num+1;
   K(Point_Num,2)=K(8,2)-Step*i;
   K(Point_Num,1)=sqrt((1+(K(Point_Num,2)-(L2-L5))^2/b2)*R6^2);
   x=K(Point_Num,1);
   y=K(Point_Num,2);
   L_k=(b2*x)/(sqrt(b2*(x^2/a^2-1))*a^2);
   th=abs(atan(L_k));
   Point_Num=Point_Num+1;
   K(Point_Num,1)=x-H1*sin(th);%(L_b1-L_b+(H1/cos(atan(L_k))))/(L_k+1/L_k);
   K(Point_Num,2)=y-H1*cos(th);%-1/L_k*K(Point_Num,1)+L_b1;
end
for i=1:1:Point_Num
    fprintf(fidin,'K,%d,%s,%s\n',i,mat2str(K(i,1)),mat2str(K(i,2)));
end
%连接直线
fprintf(fidin,'L,3,4\n');
fprintf(fidin,'L,4,5\n');
fprintf(fidin,'L,5,6\n');
fprintf(fidin,'L,7,8\n');
fprintf(fidin,'L,9,10\n');
%连接外圆
fprintf(fidin,'FLST,3,%d,3\n',Circle_Num+1);
fprintf(fidin,'FITEM,3,6\n');
for i=11:1:15
    fprintf(fidin,'FITEM,3,%d\n',i);
end
fprintf(fidin,'FITEM,3,7\n');
fprintf(fidin,'BSPLIN,,P51X\n');
%连接内圆
fprintf(fidin,'FLST,3,%d,3\n',Circle_Num+1);
fprintf(fidin,'FITEM,3,3\n');
for i=16:1:20
    fprintf(fidin,'FITEM,3,%d\n',i);
end
fprintf(fidin,'FITEM,3,10\n');
fprintf(fidin,'BSPLIN,,P51X\n');
%连接外双曲线
fprintf(fidin,'FLST,3,%d,3\n',Hyper_Num+1);
fprintf(fidin,'FITEM,3,8\n');
for i=21:2:31
    fprintf(fidin,'FITEM,3,%d\n',i);
end
fprintf(fidin,'BSPLIN,,P51X\n');
%连接外内曲线
fprintf(fidin,'FLST,3,%d,3\n',Hyper_Num+1);
fprintf(fidin,'FITEM,3,9\n');
for i=22:2:32
    fprintf(fidin,'FITEM,3,%d\n',i);
end
fprintf(fidin,'BSPLIN,,P51X\n');
fprintf(fidin,'L,31,32\n');
fprintf(fidin,'LFILLT,3,6,%s, ,\n',mat2str(R5));
fprintf(fidin,'FLST,2,11,4\n');
for i=1:1:11
   fprintf(fidin,'FITEM,2,%d\n',i);
end
fprintf(fidin,'AL,P51X\n');
%生成体
fprintf(fidin,'FLST,2,1,5,ORDE,1\n'); 
fprintf(fidin,'FITEM,2,1\n');   
fprintf(fidin,'FLST,8,2,3\n');  
fprintf(fidin,'FITEM,8,2\n');   
fprintf(fidin,'FITEM,8,1\n');   
fprintf(fidin,'VROTAT,P51X, , , , , ,P51X, ,360, ,\n'); 
fprintf(fidin,'FLST,2,4,6,ORDE,2\n');   
fprintf(fidin,'FITEM,2,1\n');    
fprintf(fidin,'FITEM,2,-4\n');  
fprintf(fidin,'VADD,P51X\n'); 
%复位所有编号
fprintf(fidin,'NUMCMP,ALL\n');
%绘制隔离孔
fprintf(fidin,'wpoff,0,%s,0\n',mat2str(GLUE_P));
for i=1:1:8
    fprintf(fidin,'!第%d个孔\n',i);
    fprintf(fidin,'CYL4,0,0,%s,,,,0.02\n',mat2str(R4));
    fprintf(fidin,'VSBV,1,2\n');
    fprintf(fidin,'NUMCMP,ALL\n');
    fprintf(fidin,'wprot,0,0,%s\n',mat2str(45));
end
% %绘制胶层和压电片
% fprintf(fidin,'wprot,0,0,%s\n',mat2str(22.5));
% fprintf(fidin,'wpoff,0,0,%s\n',mat2str(R6));
% fprintf(fidin,'BLOCK,%s,%s,%s,%s,%s,%s\n',mat2str(-GLUE_W/2),mat2str(GLUE_W/2),...
%     mat2str(-GLUE_L/2),mat2str(GLUE_L/2),mat2str(-0.005),mat2str(GLUE_H));
% fprintf(fidin,'BLOCK,%s,%s,%s,%s,%s,%s\n',mat2str(-PIE_W/2),mat2str(PIE_W/2),...
%     mat2str(-PIE_L/2),mat2str(PIE_L/2),mat2str(GLUE_H),mat2str(PIE_H+GLUE_H));
% 
% fprintf(fidin,'FLST,2,2,6,ORDE,2\n');   
% fprintf(fidin,'FITEM,2,1\n');   
% fprintf(fidin,'FITEM,2,-2\n');  
% fprintf(fidin,'VOVLAP,P51X\n');
% 
% fprintf(fidin,'VDELE,4, , ,1\n');
% fprintf(fidin,'FLST,2,2,6,ORDE,2\n');   
% fprintf(fidin,'FITEM,2,6\n');   
% fprintf(fidin,'FITEM,2,-7\n');
% fprintf(fidin,'VADD,P51X\n');
% fprintf(fidin,'NUMCMP,ALL\n');
% 
% fprintf(fidin,'FLST,3,1,6,ORDE,1\n');   
% fprintf(fidin,'FITEM,3,2\n');   
% fprintf(fidin,'VSYMM,X,P51X, , , ,0,0\n');  
% fprintf(fidin,'FLST,3,1,6,ORDE,1\n');   
% fprintf(fidin,'FITEM,3,3\n');    
% fprintf(fidin,'VSYMM,X,P51X, , , ,0,0\n');  
% fprintf(fidin,'FLST,3,2,6,ORDE,2\n');   
% fprintf(fidin,'FITEM,3,4\n');     
% fprintf(fidin,'FITEM,3,-5\n');  
% fprintf(fidin,'VSYMM,Z,P51X, , , ,0,0\n');  
%  
% fprintf(fidin,'FLST,3,1,6,ORDE,1\n');   
% fprintf(fidin,'FITEM,3,2\n');    
% fprintf(fidin,'VSYMM,Z,P51X, , , ,0,0\n');    
% fprintf(fidin,'FLST,3,1,6,ORDE,1\n');    
% fprintf(fidin,'FITEM,3,3\n');   
% fprintf(fidin,'VSYMM,Z,P51X, , , ,0,0\n');
% fprintf(fidin,'NUMCMP,ALL\n');
% 
% %坐标复原
% fprintf(fidin,'wpoff,0,0,%s\n',mat2str(-R6));
% 
% fprintf(fidin,'wprot,0,0,%s\n',mat2str(45));
% fprintf(fidin,'wpoff,0,0,%s\n',mat2str(R6));
% fprintf(fidin,'BLOCK,%s,%s,%s,%s,%s,%s\n',mat2str(-GLUE_W/2),mat2str(GLUE_W/2),...
%     mat2str(-GLUE_L/2),mat2str(GLUE_L/2),mat2str(-0.005),mat2str(GLUE_H));
% fprintf(fidin,'BLOCK,%s,%s,%s,%s,%s,%s\n',mat2str(-PIE_W/2),mat2str(PIE_W/2),...
%     mat2str(-PIE_L/2),mat2str(PIE_L/2),mat2str(GLUE_H),mat2str(PIE_H+GLUE_H));
% 
% fprintf(fidin,'FLST,2,2,6,ORDE,2\n');   
% fprintf(fidin,'FITEM,2,1\n');    
% fprintf(fidin,'FITEM,2,10\n');   
% fprintf(fidin,'VOVLAP,P51X\n');   
% fprintf(fidin,'VDELE,12, , ,1\n');
% fprintf(fidin,'FLST,2,2,6,ORDE,2\n');   
% fprintf(fidin,'FITEM,2,14\n');  
% fprintf(fidin,'FITEM,2,-15\n'); 
% fprintf(fidin,'VADD,P51X\n');  
% 
% fprintf(fidin,'FLST,3,1,6,ORDE,1\n');   
% fprintf(fidin,'FITEM,3,11\n');   
% fprintf(fidin,'VSYMM,X,P51X, , , ,0,0\n');  
% fprintf(fidin,'FLST,3,1,6,ORDE,1\n');  
% fprintf(fidin,'FITEM,3,13\n');    
% fprintf(fidin,'VSYMM,X,P51X, , , ,0,0\n');  
% fprintf(fidin,'FLST,3,1,6,ORDE,1\n');   
% fprintf(fidin,'FITEM,3,11\n');  
% fprintf(fidin,'VSYMM,Z,P51X, , , ,0,0\n');   
% fprintf(fidin,'FLST,3,1,6,ORDE,1\n');   
% fprintf(fidin,'FITEM,3,13\n');  
% fprintf(fidin,'VSYMM,Z,P51X, , , ,0,0\n');  
% fprintf(fidin,'FLST,3,1,6,ORDE,1\n');   
% fprintf(fidin,'FITEM,3,10\n');  
% fprintf(fidin,'VSYMM,Z,P51X, , , ,0,0\n');   
% fprintf(fidin,'FLST,3,1,6,ORDE,1\n');    
% fprintf(fidin,'FITEM,3,12\n');  
% fprintf(fidin,'VSYMM,Z,P51X, , , ,0,0\n'); 
 fprintf(fidin,'NUMCMP,ALL\n');

Mode=fopen('Mode_Analysis.txt');
while ~feof(Mode)
    tline=fgets(Mode);
    fprintf(fidin,'%c',tline);
end
fclose(Mode);

% Impact=fopen('Impact_Analysis.txt');
% while ~feof(Impact)
%     tline=fgets(Impact);
%     fprintf(fidin,'%c',tline);
% end
% fclose(Impact);
%% 显示输出
fclose(fidin);
plot(K(:,1),K(:,2),'*')