%% 利用多物理场耦合仿真，分析在压电片驱动下圆杯形振子的运动形式
% 创建时间：20111215
% 创建人：刘宁
% 备注：可用，已完成圆杯形+压电片，简化模型仿真。
%% 系统初始化
clear all;
close all;
clc;
%% 变量初始化
txt_str='APDL_01.txt';
save_name='APDL_02.txt';

read_apdl='CVG_MODAL.txt';
save_MODE_result='result_2011_03_04_zonghe.txt';
pic_name='CVG_MODAL';
L=[11E-3;19E-3;6.4E-3;3.7E-3;0.3E-3];
D=[1.3E-3;1.5E-3;2.5E-3;3.1E-3;4E-3;2E-3];
R=[11.4E-3;1.1E-3;11.8E-3;1E-3;0.1E-3;3.1E-3;7.2E-3;1.5E-3];
HD=0.7E-3;
P_L=0.005;   %压电片长
P_W=0.002;   %压电片宽
P_H=0.0002;  %压电片高
G_L=0.005;   %胶的长
G_W=0.002;   %胶的宽
G_H=0.0001;  %胶的高

Cal_Time=3e-3;   %计算时间,单位：s
Sample_Time=16e-6;  %输出时间间隔，即载荷步时间时间
%% 进入数据生成
CVG_CAL(pic_name,save_name,txt_str,read_apdl,L,D,R,HD,P_L,P_W,P_H,G_L,G_W,G_H,Cal_Time,Sample_Time);