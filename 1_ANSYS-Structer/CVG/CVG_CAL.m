function [] = CVG_CAL(pic_name,save_name,txt_str,read_apdl,L,D,R,HD,P_L,P_W,P_H,G_L,G_W,G_H,Cal_Time,Sample_Time)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
APDLin=fopen(txt_str,'w');
fprintf(APDLin,'FINISH\n');
fprintf(APDLin,'/CLEAR\n');
fprintf(APDLin,'/PREP7\n');
fprintf(APDLin,'*SET,L1,%s\n',mat2str(L(1)));
fprintf(APDLin,'*SET,L2,%s\n',mat2str(L(2)));
fprintf(APDLin,'*SET,L3,%s\n',mat2str(L(3)));
fprintf(APDLin,'*SET,L4,%s\n',mat2str(L(4)));
fprintf(APDLin,'*SET,L5,%s\n',mat2str(L(5)));
fprintf(APDLin,'*SET,D1,%s\n',mat2str(D(1)));
fprintf(APDLin,'*SET,D2,%s\n',mat2str(D(2)));
fprintf(APDLin,'*SET,D3,%s\n',mat2str(D(3)));
fprintf(APDLin,'*SET,D4,%s\n',mat2str(D(4)));
fprintf(APDLin,'*SET,D5,%s\n',mat2str(D(5)));
fprintf(APDLin,'*SET,D6,%s\n',mat2str(D(6)));
fprintf(APDLin,'*SET,R1,%s\n',mat2str(R(1)));
fprintf(APDLin,'*SET,R2,%s\n',mat2str(R(2)));
fprintf(APDLin,'*SET,R3,%s\n',mat2str(R(3)));
fprintf(APDLin,'*SET,R4,%s\n',mat2str(R(4)));
fprintf(APDLin,'*SET,R5,%s\n',mat2str(R(5)));
fprintf(APDLin,'*SET,R6,%s\n',mat2str(R(6)));
fprintf(APDLin,'*SET,R7,%s\n',mat2str(R(7)));
fprintf(APDLin,'*SET,R8,%s\n',mat2str(R(8)));
fprintf(APDLin,'*SET,HD,%s\n',mat2str(HD));
fprintf(APDLin,'*SET,P_L,%s\n',mat2str(P_L));
fprintf(APDLin,'*SET,P_W,%s\n',mat2str(P_W));
fprintf(APDLin,'*SET,P_H,%s\n',mat2str(P_H));
fprintf(APDLin,'*SET,G_L,%s\n',mat2str(G_L));
fprintf(APDLin,'*SET,G_W,%s\n',mat2str(G_W));
fprintf(APDLin,'*SET,G_H,%s\n',mat2str(G_H));
fprintf(APDLin,'*SET,Cal_Time,%s\n',mat2str(Cal_Time));
fprintf(APDLin,'*SET,Sample_Time,%s\n',mat2str(Sample_Time));
CVG_APDL=fopen(read_apdl);
while ~feof(CVG_APDL)
  tline=fgets(CVG_APDL);
  fprintf(APDLin,'%s',tline);
end
fclose(CVG_APDL);
% %保存模态分析数据
% fprintf(APDLin,'\n/SOL\nOUTPR,ALL\nOUTRES,ALL\n');
% fprintf(APDLin,'/OUTPUT,%s\n',save_name);  
% %求解
% fprintf(APDLin,'SOLVE\n');
% fprintf(APDLin,'FINISH\n');
% fprintf(APDLin,'/POST1\n*do,j,1,8\n');
% fprintf(APDLin,'/GRAPHICS,POWER\n'); 
% fprintf(APDLin,'/SHOW,JPEG,,0\n'); 
% fprintf(APDLin,'SET,1,j\n/VIEW,1,,-1\n');
% fprintf(APDLin,'/RGB,INDEX,100,100,100, 0\n');   
% fprintf(APDLin,'/RGB,INDEX, 80, 80, 80,13\n');   
% fprintf(APDLin,'/RGB,INDEX, 60, 60, 60,14\n');   
% fprintf(APDLin,'/RGB,INDEX, 0, 0, 0,15\n');
% fprintf(APDLin,'PLDISP,2\n'); 
% fprintf(APDLin,'/REPLOT\n'); 
% fprintf(APDLin,'/SHOW,CLOSE\n');
% fprintf(APDLin,'*enddo\n'); 
% for i=1:16
%    if i<11
%       pic_file=sprintf('file00%d',i-1);
%    else
%       pic_file=sprintf('file0%2d',i-1); 
%    end
%    if (mod(i,2)==0)
%        fprintf(APDLin,'/DELETE,%s,JPG\n',pic_file);
%    elseif (mod(i,2)==1)
%        pic_name1=sprintf('%s%s',pic_name,int2str(i/2));
%        fprintf(APDLin,'/RENAME,%s,JPG,,%s,,\n',pic_file,pic_name1); 
%    end
% end
% fprintf(APDLin,'FINISH');
fclose(APDLin);
end

