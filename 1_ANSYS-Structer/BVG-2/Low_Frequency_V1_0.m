%% 针对BVG_2型振子进行有限元仿真分析
%  文件名:BVG_2
%  版本号：V1.0
%  创建日期：20110727
%  创建人：刘宁
%% 系统初始化
clear all;
clc;
close all;
fprintf('系统初始化完毕!\n');
%% 初始化变量
fprintf('初始化相关变量！\n');
txt_str='APDL_1.txt';
ex=0.7E11;
prxy=0.3;
dens=2700;
%硬铝材料
ET=sprintf('SOLID186'); %选取单元
EX=sprintf('%f',ex); %弹性模量
PRXY=sprintf('%f',prxy);       %定义泊松比
DENS=sprintf('%f',dens);    %定义密度

R1=99E-2;
L1=40E-2;
L2=180E-2;
L3=R1+2e-2;
H1=1E-3;
H2=1E-3;
H3=15E-2;
H4=5E-3;

K(1,1)=0;
K(2,1)=0;
K(1,2)=0;
K(2,2)=L1;
K(1,3)=R1;
K(2,3)=L1;
K(1,4)=R1;
K(2,4)=L2+L1;
K(1,5)=R1+H1;
K(2,5)=L1+L2;
K(1,6)=R1+H1;
K(2,6)=L1-H2;
K(1,7)=H3;
K(2,7)=L1-H2;
K(1,8)=H3;
K(2,8)=H4;
K(1,9)=L3+H3;
K(2,9)=H4;
K(1,10)=L3+H3;
K(2,10)=0;

%生成APDL语言
fid=fopen(txt_str,'wt');
fprintf(fid,'!钟形振子式角速率陀螺---钟形谐振子抛物线剖面基本程序\n');
fprintf(fid,'FINISH\n');   %结束前时刻操作
fprintf(fid,'/CLEAR\n');
fprintf(fid,'/PREP7\n');
%定义单元属性
fprintf(fid,'ET,1,PLANE42\n');
fprintf(fid,'ET,2,%s\nMP,EX,1,%s\nMP,PRXY,1,%s\nMP,DENS,1,%s\n',ET,EX,PRXY,DENS);
%设定基本点坐标
for i=1:10
     fprintf(fid,'K,%s,%s,%s\n',int2str(i),mat2str(K(1,i)),mat2str(K(2,i))); 
end
fprintf(fid,'L,1,2\n');
fprintf(fid,'L,2,3\n');
fprintf(fid,'L,3,4\n');
fprintf(fid,'L,4,5\n');
fprintf(fid,'L,5,6\n');
fprintf(fid,'L,6,7\n');
fprintf(fid,'L,7,8\n');
fprintf(fid,'L,8,9\n');
fprintf(fid,'L,9,10\n');
fprintf(fid,'L,10,1\n');
fprintf(fid,'FLST,2,10,4\n'); 
fprintf(fid,'FITEM,2,1\n');   
fprintf(fid,'FITEM,2,10\n');  
fprintf(fid,'FITEM,2,9\n');   
fprintf(fid,'FITEM,2,8\n');   
fprintf(fid,'FITEM,2,7\n');   
fprintf(fid,'FITEM,2,2\n');   
fprintf(fid,'FITEM,2,6\n');   
fprintf(fid,'FITEM,2,3\n');   
fprintf(fid,'FITEM,2,5\n');   
fprintf(fid,'FITEM,2,4\n');   
fprintf(fid,'AL,P51X\n'); 
fprintf(fid,'FLST,2,1,5,ORDE,1\n');   
fprintf(fid,'FITEM,2,1\n');   
fprintf(fid,'FLST,8,2,3\n');  
fprintf(fid,'FITEM,8,2\n');   
fprintf(fid,'FITEM,8,1\n');   
fprintf(fid,'VROTAT,P51X, , , , , ,P51X, ,360, ,\n'); 
fclose(fid);