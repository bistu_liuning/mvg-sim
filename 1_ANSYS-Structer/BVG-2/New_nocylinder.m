%% 文件名称：用于新型旋转抛物面振子的仿真分析
%  创建日期：20110516
%  创建人：刘宁
%  软件说明： 用于中间无柱的钟形振子的仿真，便于结构设计与分析
%% 系统运行初始化
clear all;
close all;
clc;
%% 系统参数初始化
L1=8E-3;
R1=12E-3;
H1=0.8E-3;
L2=6E-3;
L3=9E-3;


L4=2E-3;
W=30;
R2=2E-3;
R3=6E-3;
R4=1.5E-3;
R5=4E-3;
%-----------------
%设定文件初始参数
txt_str='APDL.txt';
save_name='APDL.txt';
read_apdl='NEW_stru.txt';
%-----------------
%-----------------
%定义系统初值,按照新振子的材料特性参数
ex=1.9676E11;
prxy=0.3;
dens=8170;
ET=sprintf('SOLID186'); %选取单元
EX=sprintf('%f',ex); %弹性模量
PRXY=sprintf('%f',prxy);       %定义泊松比
DENS=sprintf('%f',dens);    %定义密度
%-----------------
%设定计算曲线的相关数值，
sin_Sample_num=20;  %内曲线正弦点数，即为采样数
Point_Line_num=0;   %直线总点数
Point_Sin_num=0;    %正弦总点数
Point_Sin_wai_num=0;%外曲线点数
Point_Sin_nei_num=0;%内曲线点数
Line_num=0;
Point_num=0;
%绘制外曲线各点
K(1,1)=0;
K(2,1)=L2+L3;
K(1,2)=0;
K(2,2)=L2+L3+L1;
K(1,3)=R2-L4*tand(W);
K(2,3)=L2+L3;
K(1,4)=R2;
K(2,4)=L2+L3+L4;
K(1,5)=R2;
K(2,5)=L2+L3+L1;
K(1,6)=R5;
K(2,6)=L2+L3+L1;
K(1,7)=R5;
K(2,7)=L2+L3+H1;
Point_Line_num=7;
Point_num=Point_Line_num+Point_Sin_num;
%-----------------
%绘制外曲线各点
A_wai=K(2,7)/(K(1,7)^2-R1^2);
C_wai=-A_wai*R1^2;
for i=1:sin_Sample_num   %将x轴等分，选取正弦点数
     x=R5+((R1-R5)/sin_Sample_num)*i;
     y=A_wai*x^2+C_wai;
     K(1,i+Point_Line_num)=x;
     K(2,i+Point_Line_num)=y;
     Point_Sin_nei_num=Point_Sin_nei_num+1;%记录外曲线点数
end
Point_Sin_num=Point_Sin_nei_num+Point_Sin_wai_num;
Point_num=Point_Line_num+Point_Sin_num;
WAI=Point_num;   %记录绘制外曲线前所有点的数量
for i=1:sin_Sample_num   %将x轴等分，选取正弦点数
     x=K(1,Point_Line_num+Point_Sin_nei_num-i+1);
     y=K(2,Point_Line_num+Point_Sin_nei_num-i+1);
     x=(H1+2*A_wai*x^2+1/(2*A_wai*x)*x)/(2*A_wai*x+1/(2*A_wai*x));
     y=y-H1;
     K(1,i+Point_num)=x;
     K(2,i+Point_num)=y;
     Point_Sin_wai_num=Point_Sin_wai_num+1;%记录外曲线点数
end
Point_Sin_num=Point_Sin_nei_num+Point_Sin_wai_num;
Point_num=Point_Line_num+Point_Sin_num;
%----------------------
%生成APDL语言
fid=fopen(txt_str,'wt');
fprintf(fid,'!钟形振子式角速率陀螺---钟形谐振子抛物线剖面基本程序\n');
fprintf(fid,'FINISH\n');   %结束前时刻操作
fprintf(fid,'/CLEAR\n');
fprintf(fid,'/PREP7\n');
fprintf(fid,'*SET,R3,%s\n',mat2str(R3));
fprintf(fid,'*SET,L3,%s\n',mat2str(L3));
fprintf(fid,'*SET,R4,%s\n',mat2str(R4));
%定义单元属性
fprintf(fid,'ET,1,PLANE42\n');
fprintf(fid,'ET,2,%s\nMP,EX,1,%s\nMP,PRXY,1,%s\nMP,DENS,1,%s\n',ET,EX,PRXY,DENS);
%设定基本点坐标
for i=1:Point_num
     fprintf(fid,'K,%s,%s,%s\n',int2str(i),mat2str(K(1,i)),mat2str(K(2,i))); 
end
fprintf(fid,'L,3,4\n');
Line_num=Line_num+1;
fprintf(fid,'L,4,5\n');
Line_num=Line_num+1;
fprintf(fid,'L,5,6\n');
Line_num=Line_num+1;
fprintf(fid,'L,6,7\n');
Line_num=Line_num+1;

%内曲线连接成弧
fprintf(fid,'CSYS,2\n');
fprintf(fid,'FLST,3,%s,3\n',int2str(Point_Sin_nei_num+1));
for i=7:1:Point_Sin_nei_num+7
    fprintf(fid,'FITEM,3,%s\n',int2str(i));%连接曲线
end
fprintf(fid,'BSPLIN,,P51X\n');
Line_num=Line_num+1;
fprintf(fid,'L,%s,%s\n',int2str(Point_Sin_nei_num+7),int2str(Point_Sin_nei_num+7+1));
Line_num=Line_num+1;
%外曲线连接成弧
fprintf(fid,'FLST,3,%s,3\n',int2str(Point_Sin_wai_num));
for i=Point_Sin_nei_num+7+1:1:Point_Sin_nei_num+7+1+Point_Sin_wai_num-1
    fprintf(fid,'FITEM,3,%s\n',int2str(i));%连接曲线
end
fprintf(fid,'BSPLIN,,P51X\n');
Line_num=Line_num+1;
fprintf(fid,'CSYS,0\n');
%fprintf(fid,'L,%s,%s\n',int2str(Point_Sin_nei_num+7+1+Point_Sin_wai_num-1),int2str(3));
Line_num=Line_num+1;
K(1,48)=K(1,4)+(K(1,6)-K(1,5))/2;
K(2,48)=0;
K(1,49)=0;
K(2,49)=0;
K(1,50)=K(1,48);
K(2,50)=K(2,47);
fprintf(fid,'K,48,%s,%s\n',mat2str(K(1,48)),mat2str(K(2,48))); 
fprintf(fid,'K,49,%s,%s\n',mat2str(K(1,49)),mat2str(K(2,49)));
fprintf(fid,'K,50,%s,%s\n',mat2str(K(1,50)),mat2str(K(2,50)));
fprintf(fid,'L,50,48\n');
fprintf(fid,'L,50,47\n');
fprintf(fid,'L,48,49\n');
fprintf(fid,'L,49,1\n');
fprintf(fid,'L,1,3\n');
fprintf(fid,'LFILLT,9,8,0.0008, ,\n');


CVG_APDL=fopen(read_apdl);
while ~feof(CVG_APDL)
  tline=fgets(CVG_APDL);
  fprintf(fid,'%s',tline);
end
fclose(CVG_APDL);
% %保存模态分析数据
% fprintf(fid,'\n/SOL\nOUTPR,ALL\nOUTRES,ALL\n');
% fprintf(fid,'/OUTPUT,%s\n',save_name);  
% %求解
% fprintf(fid,'SOLVE\n');
% fprintf(fid,'FINISH\n');
% fprintf(fid,'/POST1\n*do,j,1,8\n');
% fprintf(fid,'/GRAPHICS,POWER\n'); 
% fprintf(fid,'/SHOW,JPEG,,0\n'); 
% fprintf(fid,'SET,1,j\n/VIEW,1,,-1\n');
% fprintf(fid,'/RGB,INDEX,100,100,100, 0\n');   
% fprintf(fid,'/RGB,INDEX, 80, 80, 80,13\n');   
% fprintf(fid,'/RGB,INDEX, 60, 60, 60,14\n');   
% fprintf(fid,'/RGB,INDEX, 0, 0, 0,15\n');
% fprintf(fid,'PLDISP,2\n'); 
% fprintf(fid,'/REPLOT\n'); 
% fprintf(fid,'/SHOW,CLOSE\n');
% fprintf(fid,'*enddo\n'); 
% for i=1:16
%    if i<11
%       pic_file=sprintf('file00%d',i-1);
%    else
%       pic_file=sprintf('file0%2d',i-1); 
%    end
%    if (mod(i,2)==0)
%        fprintf(fid,'/DELETE,%s,JPG\n',pic_file);
%    elseif (mod(i,2)==1)
%        pic_name1=sprintf('%s%s',pic_name,int2str(i/2));
%        fprintf(fid,'/RENAME,%s,JPG,,%s,,\n',pic_file,pic_name1); 
%    end
% end
% fprintf(fid,'FINISH');
fclose(fid);
