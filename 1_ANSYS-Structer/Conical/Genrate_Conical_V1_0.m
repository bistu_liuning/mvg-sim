%% 圆锥形振子的有限元仿真及参数分析
% 创建日期：20130320
% 创建人：刘宁
% 版本：1.0
% 说明：实现圆锥形振子的有限元模型建立的APDL语言
% 版权：北京信息科技大学智能控制研究所，北京理工大学自动化学院

%% 系统初始化
clear all;
clc;
close all;
%% 算法初始化
% 显示圆锥壳参数定义
figure(1);
[Conical_Pic, cmap]=imread('Conical_pic.jpg');
imshow(Conical_Pic);
% 定义圆锥壳使用参数
pi=3.1415926;
theta=25*pi/180; %圆锥角,单位度
L1=20E-3;     %圆锥高
L2=16E-3;
L3=0.6E-3;
L4=8E-3;
H1=0.6e-3;
H2=0.9e-3;
R1=2.5E-3;

R2=2E-3;
R5=11E-3;     %圆锥底部半径
R6=1E-3;      %隔离孔半径
R7=0.1E-3;
R8=0.5E-3;
L6=(8e-3)/cos(theta)+H2*sin(theta);%%%%

GLUE_P=L6;    %粘胶位置，距底部高度
PIE_P=GLUE_P; %压电片位置，距底部高度
PIE_L=8e-3;   %压电片高
PIE_W=2E-3;   %压电片宽
PIE_H=0.2E-3; %压电片厚度
GLUE_L=PIE_L; %胶层高
GLUE_W=PIE_W; %胶层宽
GLUE_H=0.6E-3;%胶层厚度
%% 计入算法
fidin=fopen('Conical_APDL.txt','wt');
fprintf(fidin,'FINISH\n');
fprintf(fidin,'/CLEAR\n');
%打开ANSYS建模模块
fprintf(fidin,'/PREP7\n');
%定义材料参数
K(1,1)=0;  %1为X 2为Y
K(1,2)=L2;
K(2,1)=R1;
K(2,2)=L2;
K(3,1)=R2;
K(3,2)=L1-L3;
K(4,1)=R2;
K(4,2)=L1;
K(8,1)=R5+H2*cos(theta);
K(8,2)=H2*sin(theta);
K(7,2)=K(8,2)+L4*cos(theta);
K(7,1)=K(8,1)-L4*sin(theta);
K(6,1)=K(7,1)-(H2-H1)*cos(theta);
K(6,2)=K(7,2)-(H2-H1)*sin(theta);
R3=K(6,1)-(L1-K(6,2))*tan(theta);
K(5,1)=R3;
K(5,2)=L1;
K(9,1)=R5;
K(9,2)=0;
K(10,1)=R5-L2*tan(theta);
K(10,2)=L2;
K(11,1)=0;
K(11,2)=0;
%% 运行ANSYS计算
[Point_Num,Colum]=size(K);
% 绘制关键点
for i=1:1:Point_Num
    str=sprintf('K,%d,%%s,%%s\n',i);
    fprintf(fidin,str,mat2str(K(i,1)),mat2str(K(i,2)));
end
%连接曲线
Line_Num=Point_Num-2;
for i=1:1:Line_Num-1
    fprintf(fidin,'L,%d,%d\n',i+1,i+2);
end
fprintf(fidin,'L,%d,2\n',Point_Num-1);
% 绘制倒角
fprintf(fidin,'LFILLT,4,5,%s\n',mat2str(R7));
Line_Num=Line_Num+1;
fprintf(fidin,'LFILLT,8,9,%s\n',mat2str(R8));
Line_Num=Line_Num+1;
% 生成面
fprintf(fidin,'FLST,2,%d,4\n',Line_Num);
for i=1:1:Line_Num
    fprintf(fidin,'FITEM,2,%d\n',i);
end
fprintf(fidin,'AL,P51X\n');
fprintf(fidin,'FLST,2,1,5,ORDE,1\n');   
fprintf(fidin,'FITEM,2,1\n');   
fprintf(fidin,'FLST,8,2,3\n');     
fprintf(fidin,'FITEM,8,1\n');    
fprintf(fidin,'FITEM,8,11\n');   
fprintf(fidin,'VROTAT,P51X, , , , , ,P51X, ,360, ,\n');  
% 生成一个体积元
fprintf(fidin,'FLST,2,4,6,ORDE,2\n');  
fprintf(fidin,'FITEM,2,1\n');   
fprintf(fidin,'FITEM,2,-4\n');    
fprintf(fidin,'VADD,P51X\n');  
% 重置所有编号
fprintf(fidin,'NUMCMP,ALL\n');
%% 绘制隔离孔
% 平移坐标原点
fprintf(fidin,'wpoff,0,%s,0\n',mat2str(L6));
for i=1:1:8
    fprintf(fidin,'!第%d个孔\n',i);
    fprintf(fidin,'wprot,0,%s,0\n',mat2str(-theta*180/pi));
    fprintf(fidin,'CYL4,0,0,%s,,,,0.02\n',mat2str(R6));
    fprintf(fidin,'VSBV,1,2\n');  %生成孔
    % 坐标复原
    fprintf(fidin,'wprot,0,%s,0\n',mat2str(theta*180/pi));
    % 重置所有编号
    fprintf(fidin,'NUMCMP,ALL\n');
    fprintf(fidin,'wprot,0,0,%s\n',mat2str(45));
end
fprintf(fidin,'wpoff,0,%s,0\n',mat2str(-L6));
% %% 绘制胶层和压电片
fprintf(fidin,'wpoff,0,%s,0\n',mat2str(GLUE_P));
fprintf(fidin,'wprot,0,0,%s\n',mat2str(22.5));
fprintf(fidin,'wprot,0,%s,0\n',mat2str(-theta*180/pi));
fprintf(fidin,'wpoff,0,0,%s\n',mat2str(H1+(R5-GLUE_P*tan(theta))*cos(theta)));
fprintf(fidin,'BLOCK,%s,%s,%s,%s,%s,%s\n',mat2str(-GLUE_W/2),mat2str(GLUE_W/2),...
    mat2str(-GLUE_L/2),mat2str(GLUE_L/2),mat2str(-0.005),mat2str(GLUE_H));
fprintf(fidin,'BLOCK,%s,%s,%s,%s,%s,%s\n',mat2str(-PIE_W/2),mat2str(PIE_W/2),...
    mat2str(-PIE_L/2),mat2str(PIE_L/2),mat2str(GLUE_H),mat2str(PIE_H+GLUE_H));

fprintf(fidin,'FLST,2,2,6,ORDE,2\n');   
fprintf(fidin,'FITEM,2,1\n');   
fprintf(fidin,'FITEM,2,-2\n');  
fprintf(fidin,'VOVLAP,P51X\n');

fprintf(fidin,'VDELE,4, , ,1\n');
fprintf(fidin,'FLST,2,2,6,ORDE,2\n');   
fprintf(fidin,'FITEM,2,6\n');   
fprintf(fidin,'FITEM,2,-7\n');
fprintf(fidin,'VADD,P51X\n');
%坐标复原
fprintf(fidin,'wpoff,0,0,%s\n',mat2str(-(H1+(R5-GLUE_P*tan(theta))*cos(theta))));
fprintf(fidin,'wprot,0,%s,0\n',mat2str(theta*180/pi));
fprintf(fidin,'wprot,0,0,%s\n',mat2str(-22.5));
fprintf(fidin,'NUMCMP,ALL\n');

fprintf(fidin,'FLST,3,1,6,ORDE,1\n');   
fprintf(fidin,'FITEM,3,2\n');   
fprintf(fidin,'VSYMM,X,P51X, , , ,0,0\n');  
fprintf(fidin,'FLST,3,1,6,ORDE,1\n');   
fprintf(fidin,'FITEM,3,3\n');    
fprintf(fidin,'VSYMM,X,P51X, , , ,0,0\n');  
fprintf(fidin,'FLST,3,2,6,ORDE,2\n');   
fprintf(fidin,'FITEM,3,4\n');     
fprintf(fidin,'FITEM,3,-5\n');  
fprintf(fidin,'VSYMM,Z,P51X, , , ,0,0\n');  
 
fprintf(fidin,'FLST,3,1,6,ORDE,1\n');   
fprintf(fidin,'FITEM,3,2\n');    
fprintf(fidin,'VSYMM,Z,P51X, , , ,0,0\n');    
fprintf(fidin,'FLST,3,1,6,ORDE,1\n');    
fprintf(fidin,'FITEM,3,3\n');   
fprintf(fidin,'VSYMM,Z,P51X, , , ,0,0\n');
fprintf(fidin,'NUMCMP,ALL\n');

fprintf(fidin,'wprot,0,0,%s\n',mat2str(67.5));
fprintf(fidin,'wprot,0,%s,0\n',mat2str(-theta*180/pi));
fprintf(fidin,'wpoff,0,0,%s\n',mat2str(H1+(R5-GLUE_P*tan(theta))*cos(theta)));
fprintf(fidin,'BLOCK,%s,%s,%s,%s,%s,%s\n',mat2str(-GLUE_W/2),mat2str(GLUE_W/2),...
    mat2str(-GLUE_L/2),mat2str(GLUE_L/2),mat2str(-0.005),mat2str(GLUE_H));
fprintf(fidin,'BLOCK,%s,%s,%s,%s,%s,%s\n',mat2str(-PIE_W/2),mat2str(PIE_W/2),...
    mat2str(-PIE_L/2),mat2str(PIE_L/2),mat2str(GLUE_H),mat2str(PIE_H+GLUE_H));

fprintf(fidin,'FLST,2,2,6,ORDE,2\n');   
fprintf(fidin,'FITEM,2,1\n');    
fprintf(fidin,'FITEM,2,10\n');   
fprintf(fidin,'VOVLAP,P51X\n');   
fprintf(fidin,'VDELE,12, , ,1\n');
fprintf(fidin,'FLST,2,2,6,ORDE,2\n');   
fprintf(fidin,'FITEM,2,14\n');  
fprintf(fidin,'FITEM,2,-15\n'); 
fprintf(fidin,'VADD,P51X\n');  

fprintf(fidin,'FLST,3,1,6,ORDE,1\n');   
fprintf(fidin,'FITEM,3,11\n');   
fprintf(fidin,'VSYMM,X,P51X, , , ,0,0\n');  
fprintf(fidin,'FLST,3,1,6,ORDE,1\n');  
fprintf(fidin,'FITEM,3,13\n');    
fprintf(fidin,'VSYMM,X,P51X, , , ,0,0\n');  
fprintf(fidin,'FLST,3,1,6,ORDE,1\n');   
fprintf(fidin,'FITEM,3,11\n');  
fprintf(fidin,'VSYMM,Z,P51X, , , ,0,0\n');   
fprintf(fidin,'FLST,3,1,6,ORDE,1\n');   
fprintf(fidin,'FITEM,3,13\n');  
fprintf(fidin,'VSYMM,Z,P51X, , , ,0,0\n');  
fprintf(fidin,'FLST,3,1,6,ORDE,1\n');   
fprintf(fidin,'FITEM,3,10\n');  
fprintf(fidin,'VSYMM,Z,P51X, , , ,0,0\n');   
fprintf(fidin,'FLST,3,1,6,ORDE,1\n');    
fprintf(fidin,'FITEM,3,12\n');  
fprintf(fidin,'VSYMM,Z,P51X, , , ,0,0\n'); 
fprintf(fidin,'NUMCMP,ALL\n');
% 定义材料参数
% Material=fopen('Material.txt');
% while ~feof(Material)
%     tline=fgets(Material);
%     fprintf(fidin,'%s',tline);
% end
% fclose(Material);
% 
% Mesh=fopen('Mesh.txt');
% while ~feof(Mesh)
%     tline=fgets(Mesh);
%     fprintf(fidin,'%c',tline);
% end
% fclose(Mesh);

% Mode=fopen('Mode_Analysis.txt');
% while ~feof(Mode)
%     tline=fgets(Mode);
%     fprintf(fidin,'%c',tline);
% end
% fclose(Mode);
%% 显示输出
figure(2)
plot(K(2:10,1),K(2:10,2));
hold on;
plot(K(2:10,1),K(2:10,2),'*');
%% 保存结果
fclose(fidin);