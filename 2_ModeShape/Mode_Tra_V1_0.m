%% 记录模态轨迹
% 系统初始化
clear all;
close all;
clc;
% 开始计算
n=2;
m='Det';
Theta=0:pi/100:2*pi; %设定角度
len=length(Theta);
Num=10;
Mag=0.02;
x=zeros(Num,len);
y=zeros(Num,len);
point=zeros(Num);
figure(1)
for i=1:1:Num
    x(i,:)=Mag*(2+i*0.1)*cos(n*(Theta));
    y(i,:)=Mag*(3-i*0.1)*sin(n*(Theta));
    x2(i,:)=x(i,:).*cos(pi/4)+y(i,:).*sin(pi/4);
    y2(i,:)=y(i,:).*cos(pi/4)-x(i,:).*sin(pi/4);
    if m=='Ext'
        xt=x(i,:);
        yt=y(i,:);        
    elseif m=='Det'
        xt=x(i,:).*cos(pi/4)+y(i,:).*sin(pi/4);
        yt=y(i,:).*cos(pi/4)-x(i,:).*sin(pi/4);
    end
    plot(xt,yt,'*');
    hold on;
end
grid on;
xlabel('模态分析x轴幅值');
ylabel('模态分析y轴幅值');
axis equal;
% 显示输出
figure(2)
plot(Theta, x(Num,:),'r');
hold on;
plot(Theta, y(Num,:),'b');
x1=x';
y1=y';
x3=x2';
y3=y2';
% figure(3)
% plot(point,y(:,20))
% xout=x';
% yout=y';