%% 计算钟形振子振子
% 系统初始化
close all;
clc;
clear all;
%进行振型选择 n=1->n=4
Mode='n=4';

%% 定义结构参数
L1=0; %待定
L2=22E-3;  %振子整体高度
L3=15.8E-3;  %隔离孔距底部边缘高度
L4=3E-3;
L5=20E-3;
R1=10E-3;
R2=2E-3;
R3=1.5E-3;
R4=1.5E-3;
R5=4E-3;
R6=9E-3;
R7=2.5E-3;
R8=2.2E-3;
H1=0.5E-3;

GLUE_P=L2-L3; %粘胶位置，距底部高度
PIE_P=GLUE_P; %压电片位置，距底部高度
PIE_L=8e-3;   %压电片长
PIE_W=2E-3;   %压电片宽
PIE_H=0.2E-3; %压电片厚度
GLUE_L=PIE_L; %胶层长
GLUE_W=PIE_W; %胶层宽
GLUE_H=0.2E-3;%胶层厚度

%% 绘制关键点
K(1,1)=0;
K(1,2)=0;
K(2,1)=0;
K(2,2)=L2-L4-sqrt(R6^2-R7^2);
K(3,1)=R2;
K(3,2)=K(2,2)+sqrt((R6-H1)^2-R2^2);
K(4,1)=R3;
K(4,2)=L2;
K(5,1)=R8;
K(5,2)=L2;
K(6,1)=R7;
K(6,2)=L2-L4;
K(7,1)=R6;
K(7,2)=K(2,2);
K(8,1)=R6;
K(8,2)=L2-L5;
K(9,1)=R6-H1;
K(9,2)=L2-L5;
K(10,1)=R6-H1;
K(10,2)=K(2,2);
Point_Num=10;
%绘制外圆
Circle_Num=6;
Step=(K(6,2)-K(7,2))/Circle_Num;
for i=1:1:Circle_Num-1
    Point_Num=Point_Num+1;
    K(Point_Num,2)=K(6,2)-Step*i;
    K(Point_Num,1)=sqrt(R6^2-(K(Point_Num,2)-K(2,2))^2);
end
%绘制内圆
Step=(K(3,2)-K(10,2))/Circle_Num;
for i=1:1:Circle_Num-1
    Point_Num=Point_Num+1;
    K(Point_Num,2)=K(3,2)-Step*i;
    K(Point_Num,1)=sqrt((R6-H1)^2-(K(Point_Num,2)-K(2,2))^2);
end
%绘制双曲线
a=R6;  %外侧
b2=((H1-L2+L5)^2)/((R1^2/R6^2)-1);
Hyper_Num=6;
Step=(K(8,2))/Hyper_Num;
for i=1:1:Hyper_Num
   Point_Num=Point_Num+1;
   K(Point_Num,2)=K(8,2)-Step*i;
   K(Point_Num,1)=sqrt((1+(K(Point_Num,2)-(L2-L5))^2/b2)*R6^2);
   x=K(Point_Num,1);
   y=K(Point_Num,2);
   L_k=(b2*x)/(sqrt(b2*(x^2/a^2-1))*a^2);
   th=abs(atan(L_k));
   Point_Num=Point_Num+1;
   K(Point_Num,1)=x-H1*sin(th);%(L_b1-L_b+(H1/cos(atan(L_k))))/(L_k+1/L_k);
   K(Point_Num,2)=y-H1*cos(th);%-1/L_k*K(Point_Num,1)+L_b1;
end
%% 计算纵坐标
Resonator_Height(1,1)=K(4,1);
Resonator_Height(1,2)=K(4,2);
Resonator_Height(2,1)=K(3,1);
Resonator_Height(2,2)=K(3,2);
Num=3;
for i=16:1:20
    Resonator_Height(Num,1)=K(i,1);
    Resonator_Height(Num,2)=K(i,2);
    Num=Num+1;
end
Resonator_Height(Num,1)=K(10,1);
Resonator_Height(Num,2)=K(10,2);
Num=Num+1;
Resonator_Height(Num,1)=K(9,1);
Resonator_Height(Num,2)=K(9,2);
Num=Num+1;
for i=22:2:32
    Resonator_Height(Num,1)=K(i,1);
    Resonator_Height(Num,2)=K(i,2);
    Num=Num+1;
end

%% 绘制三维图
theta=0:pi/60:2*pi;
for i=1:1:Num-1
    if i>2        
        if Mode=='n=1'
        x1(i,:)=Resonator_Height(i,1)*1000*cos(theta);
        y1(i,:)=Resonator_Height(i,1)*1000*sin(theta);
        elseif  Mode=='n=2'
        x1(i,:)=Resonator_Height(i,1)*1000*cos(theta)*1.5;
        y1(i,:)=Resonator_Height(i,1)*1000*sin(theta);           
        elseif  Mode=='n=3'
        x1(i,:)=Resonator_Height(i,1)*1000*cos(theta).*cos(3*theta);
        y1(i,:)=Resonator_Height(i,1)*1000*sin(theta).*cos(3*theta);        
        elseif  Mode=='n=4'
        x1(i,:)=Resonator_Height(i,1)*1000*cos(theta).*cos(2*theta);
        y1(i,:)=Resonator_Height(i,1)*1000*sin(theta).*cos(2*theta);     
        elseif  Mode=='n=5'
        x1(i,:)=Resonator_Height(i,1)*1000*cos(theta).*cos(theta);
        y1(i,:)=Resonator_Height(i,1)*1000*sin(theta).*cos(theta); 
        end
    else
        x1(i,:)=Resonator_Height(i,1)*1000*cos(theta);
        y1(i,:)=Resonator_Height(i,1)*1000*sin(theta);
    end
    z1(i,:)=Resonator_Height(i,2)*1000*ones(1,121);
end
mesh(x1,y1,z1);
xlabel('x');
ylabel('y');
zlabel('z');
axis equal;
x2=x1;
x3=x1;
x4=x1;

figure(2)
Edge_X=x1(Num-1,:);
Edge_Y=y1(Num-1,:);
Edge_Theta=theta;
plot(theta,Edge_Y);
figure(3)
polar(Edge_Theta,Edge_X)

% 显示输出
% 数据记录